import React, { useCallback } from 'react'
import { useSearch } from 'store/SearchContext'

import './Search.scss'

export default function Search(props) {
  const { search, onChange } = useSearch()
  const handleChange = useCallback(onChange, [onChange])

  return (
    <form className="Search">
      <input
        className="Search-input"
        placeholder="Search documents..."
        value={search}
        onChange={handleChange}
      />
    </form>
  )
}