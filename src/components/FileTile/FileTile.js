import React, { useCallback, useState } from 'react'
import { useStorage } from 'store/StorageContext'

import './FileTile.scss'

export default function FileTile(props) {
  const { removeDocument } = useStorage()
  const [error, setError] = useState(null)
  const [isLoading, setLoading] = useState(false)

  const handleClick = useCallback(async () => {
    setLoading(true)
    try {
      await removeDocument(props.name)
      setError(null)
    } catch (e) {
      setError(e.message)
    }
    setLoading(true)
  }, [props.name, removeDocument])

  return (
    <div className="FileTile">
      <h3 className="FileTile-title">
        {props.name}
      </h3>
      <div className="FileTile-actions">
        <span className="FileTile-size">
          {props.size / 1000} kb
        </span>
        <button className="FileTile-delete" onClick={handleClick}>
          {isLoading ? 'Loading' : 'Delete'}
        </button>
      </div>
      {error && (
        <p className="FileTile-error">
          {error}
        </p>
      )}
    </div>
  )
}