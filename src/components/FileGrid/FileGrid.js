import React from 'react'

import './FileGrid.scss'

function sum(files) {
  return files.reduce((total, next) => total + next.size / 1000, 0)
}

export default function FileGrid(props) {
  return (
    <main className="FileGrid">
      <header className="FileGrid-header">
        <h2 className="FileGrid-title">
          {props.documents.length} Documents
        </h2>
        <span className="FileGrid-size">
          Total Size: {sum(props.documents)}kb
        </span>
      </header>
      <div className="FileGrid-container">
        {props.children}
      </div>
    </main>
  )
}