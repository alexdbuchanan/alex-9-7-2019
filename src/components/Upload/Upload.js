import React, { useState } from 'react'
import { useStorage } from 'store/StorageContext'

import './Upload.scss'

function Upload(props) {
  const { addDocument } = useStorage()
  const [error, setError] = useState(null)
  const [isLoading, setLoading] = useState(false)


  async function handleChange(e) {
    const [file] = Array.from(e.currentTarget.files)
    
    setLoading(true)
    
    try {
      await addDocument(file)
      setError(null)
    } catch(error) {
      setError(error.message)
    }

    setLoading(false)
  }


  return (
    <div className="Upload-wrapper">
      <div
        className={'Upload-container'}
      >
        <label
          role="button"
          tabIndex="0"
          className="Upload"
        >
          <input
            className="Upload-input"
            type="file"
            onChange={handleChange}
            disabled={isLoading}
          />
          {isLoading ? (
            'Loading'
          ) : (
              'Upload Document'
            )}
        </label>
      </div>
      <div className="Upload-errorContainer">
        {error && (
          <p className="Upload-error">
            {error}
          </p>
        )}
      </div>
    </div>
  )
}


export default Upload