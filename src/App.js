import React, { useEffect, useCallback } from 'react';
import { useStorage } from 'store/StorageContext'
import { FileGrid, FileTile, Header, Search, Upload } from './components'

function App() {
  const { documents, getDocuments } = useStorage()
  const fetchDocuments = useCallback(getDocuments, [])

  useEffect(() => {
    fetchDocuments()
  }, [fetchDocuments])

  return (
    <div className="App">
      <Header>
        <Search />
        <Upload />
      </Header>
      <FileGrid documents={documents} s>
        {documents.map(file => (
          <FileTile {...file} name={file.key} key={file.eTag || file.lastModified} />
        ))}
      </FileGrid>
    </div>
  );
}

export default App;
