import React, { createContext, useContext, useState } from 'react';

export const SearchContext = createContext(['', () => { }])

export const SearchProvider = (props) => {
  const [state, setState] = useState('');

  return (
    <SearchContext.Provider value={[state, setState]}>
      {props.children}
    </SearchContext.Provider>
  );
}

export function useSearch() {
  const [value, setState] = useContext(SearchContext)

  function onChange(e) {
    e.persist()
    setState(state => e.target.value)
  }

  return {
    value,
    onChange
  }
}
