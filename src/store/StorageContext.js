import React, { createContext, useContext, useState } from 'react';
import { SearchContext } from './SearchContext'
import { Storage } from 'aws-amplify'

const StorageContext = createContext([[], () => { }])

export const StorageProvider = (props) => {
  const [state, setState] = useState([]);

  return (
    <StorageContext.Provider value={[state, setState]}>
      {props.children}
    </StorageContext.Provider>
  );
}


export function useStorage() {
  const [state, setState] = useContext(StorageContext);
  const [value] = useContext(SearchContext);

  let documents = value ? state.filter(d => d.key.toLowerCase().includes(value.toLowerCase())) : state

  function getDocuments() {
    Storage.list('')
      .then(data => setState(state => ([...state, ...data])))
      .catch(console.error)
  }

  async function addDocument(file) {
    const names = new Set(state.map(file => file.key))

    if (file.size / 1000000 > 10) {
      throw new Error("File size must be less than 10MB.")
    }

    if (!["image/jpeg", "image/png"].includes(file.type)) {
      throw new Error('Invalid file type.')
    }

    if (names.has(file.name)) {
      throw new Error('Document name already exists.')
    }

    const result = await Storage.put(file.name, file)
    setState(state => ([...state, { ...result, size: file.size, lastModified: file.lastModified }]))
    return result
  }

  async function removeDocument(fileName) {
    const result = await Storage.remove(fileName)
    setState(state => state.filter(file => file.key !== fileName))
    return result
  }

  return {
    documents,
    getDocuments,
    addDocument,
    removeDocument,
  }
}