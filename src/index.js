import React from 'react';
import Amplify from 'aws-amplify';
import ReactDOM from 'react-dom';

import awsconfig from './aws-exports';
import { StorageProvider } from 'store/StorageContext'
import { SearchProvider } from 'store/SearchContext'

import './index.css';
import App from './App';

import * as serviceWorker from './serviceWorker';

Amplify.configure(awsconfig)

ReactDOM.render(
  <StorageProvider>
    <SearchProvider>
      <App />
    </SearchProvider>
  </StorageProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
