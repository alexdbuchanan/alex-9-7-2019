import React from 'react';
import { act } from 'react-dom/test-utils';
import { mount } from 'enzyme';
import App from './App';
import { StorageProvider } from 'store/StorageContext'
import { SearchProvider } from 'store/SearchContext'

jest.mock('aws-amplify', () => {
  return {
    Storage: {
      list: jest.fn().mockResolvedValue([
        { size: '10000', key: 'alex.jpg', eTag: '12345' },
        { size: '20000', key: 'sam.jpg', eTag: '123456' }
      ])
    }
  }
})

function flush() {
  return new Promise(res => {
    setTimeout(res, 0)
  })
}

const render = (overrideProps) => {
  return mount(
    <StorageProvider>
      <SearchProvider>
        <App />
      </SearchProvider>
    </StorageProvider>
  )
}

describe('App', () => {
  let wrapper

  it('renders without crashing', async () => {
    await act(async () => {
      wrapper = render()

      await flush()
    })

    expect(wrapper.length).toBe(1)
  });

  it('renders tiles', async () => {
    await act(async () => {
      wrapper = render()

      await flush()
      wrapper.update()
    })

    const tiles = wrapper.find('.FileTile')

    expect(tiles.length).toBe(2)
  });

  it('should calculate the total kb of the files', async () => {
    await act(async () => {
      wrapper = render()

      await flush()
      wrapper.update()
    })
    
    const size = wrapper.find('.FileGrid-size').text()

    expect(size).toBe('Total Size: 30kb')
  });

})