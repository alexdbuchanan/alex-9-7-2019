# Alex - 9/7/2019
## Installation 
  - Refer to create-react-app documentation
## Security 
  - Addressed: 
    - None really. It's an unauthenticated application.
  - Concerns
    - S3 IAM policy could definitely be improved
    - Could use pre-signed S3 URLs
    - Not currently being served over HTTPS
## Improvements 
  - Fuzzy search
  - Filtering/sorting based on size, name, uploader, etc.
  - Generally better UI and transitions related to loading state and empty state.
## 
  - AWS - Amplify - It set up the S3 Bucket and Hosting for the application. It's an absolutely HUGE library, so in general I would try to only import the pieces that I was using in conjunction with lazy-loading
## API 
  - Refer to AWS Amplify Storage documentation

## Other
  - http://kraken-20190907115935-hostingbucket-dev.s3-website-us-east-1.amazonaws.com/